local awful = require("awful")

local autostart_commands = {
    "discord",
    "zoom",
--    "google-chrome-stable",
    "slack",
    "youtube-music",
    "anyconnect"
}

for cmd = 1, #autostart_commands do
    awful.spawn.once(autostart_commands[cmd])
end
