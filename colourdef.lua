-- Set html style tags for widget colors

colbegin = function(colour) return "<span color='" .. colour .. "'>" end
colend  = "</span>"
colwrap = function(colour)
    return function(text)
        return colbegin(colour) .. text .. colend
    end
end

colemph = colwrap(theme.emph)
colbody = colwrap(theme.body)
colcomment = colwrap(theme.comment)
colbackhigh = colwrap(theme.backhigh)
colback = colwrap(theme.back)

colred = colwrap(theme.red)
colgre = colwrap(theme.gre)
colyel = colwrap(theme.yel)
colblu = colwrap(theme.blu)
colmag = colwrap(theme.mag)
colcya = colwrap(theme.cya)
colora = colwrap(theme.ora)
colvio = colwrap(theme.vio)

