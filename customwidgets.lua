local widget = require("wibox.widget")
local naughty = require("naughty")
local wibox = require("wibox")
local awful = require("awful")
require("colourdef")

-- Create eye warning widget

eyewidget = widget.textbox()

local eyetimeout = 1200

function display_eye_warning()
    for s = 1, screen.count() do
	eyepopup = naughty.notify({
	    title = "Rest your eyes",
	    text = "Look outside for 20s",
	    font = "Terminus 32",
	    timeout = 30,
	    screen = s,
	    position = "top_left",
	    })
    end
end

function update_eye_widget()
    if eyetimeout <= 0 then
	display_eye_warning()
	eyetimeout = 1200
    else
	eyetimeout = eyetimeout - 5
	eyewidget:set_markup("break in " .. string.format("%.2d:%.2d", math.floor(eyetimeout / 60), eyetimeout%60 ) .. " min ")
    end
end

--eyetimer = timer{ timeout = 5}
--eyetimer:connect_signal("timeout", update_eye_widget)
--eyetimer:start()

-- Keyboard map indicator and changer
kbdcfg = {}
kbdcfg.cmd = "setxkbmap"
kbdcfg.layout = { { "dvorak", "gb", "dvorak" }, { "gb", " ", "qwerty" }, { "us", "dvp", "programmers dvorak" } }
kbdcfg.current = 3  -- dvorak is our default layout
kbdcfg.widget = wibox.widget.textbox()
kbdcfg.widget:set_text(" " .. kbdcfg.layout[kbdcfg.current][3] .. " ")
kbdcfg.switch = function ()
	kbdcfg.current = kbdcfg.current % #(kbdcfg.layout) + 1
	local t = kbdcfg.layout[kbdcfg.current]
	kbdcfg.widget:set_text(" " .. t[3] .. " ")
	awful.util.spawn_with_shell(kbdcfg.cmd .. " -layout " .. t[1] .. " -variant " .. t[2] .. "")
end

-- Mouse bindings
kbdcfg.widget:buttons(
	awful.util.table.join(awful.button({ }, 1, function () kbdcfg.switch() end))
)

