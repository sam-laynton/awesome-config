local awful = require("awful")
require("wallpaper")
-- Menu config bit

local awesomemenu = {
	{ "manual", terminal .. " -e man awesome" },
	{ "edit config", editor_cmd .. " " .. awful.util.getdir("config") .. "/rc.lua" },
	{ "restart", awesome.restart }
	}
local frost_awesome = {{ "quit", awesome.quit }}
local ubuntu_awesome = {{ "logout", "/usr/lib/indicator-session/gtk-logout-helper --logout" }}
local silver_awesome = {{ "logout", awesome.quit }}

local adminmenu = {
	{ "terminal", terminal },
	{ "htop", terminal .. " -e htop" }
	}
local ubuntu_admin = {{ "windows", "vmplayer /work/Virtual_Machines/win7vm/Windows 7.vmx" }}

local webmenu = {
	{ "firefox", "firefox" }
	}
local frost_web = {{ "chromium", "chromium" }}
local silver_web = {{ "chromium", "chromium" }}
local ubuntu_web = {{ "chromium", "chromium-browser" }}

local immenu = {
	{ "skype", "skype" },
	{ "pidgin", "pidgin" }
	}

local mediamenu = {{ "vlc", "vlc" }}
local frost_media = {
	{ "ncmpcpp", terminal .. " -e ncmpcpp" },
	{ "easy tag", "easytag" },
	{ "cinelerra", "cinelerra" },
	{ "openshot", "openshot" },
	{ "mixer-alsa", terminal .. " -e alsamixer" }
	}
local ubuntu_media = {{ "mixer-pavucontrol", "pavucontrol" }}
local silver_media = {{ "mixer", "pavucontrol" }}

local docsmenu = {
	{ "vim", terminal .. " -e vim"},
	{ "gvim", "gvim"},
	{ "zathura", "zathura" }
	}
local frost_docs = {{ "calibre", "calibre" }}
local silver_docs = {{ "calibre", "calibre" }}

local graphicsmenu = {
	{ "gimp", "gimp" },
	{ "dia", "dia" }
	}
local frost_graphics = {{ "digikam", "digikam" }}
local silver_graphics = {{ "digikam", "digikam" }}

local officemenu = {
	{ "menu" , "libreoffice" },
	{ "writer" , "libreoffice --writer"},
	{ "calc" , "libreoffice --calc"},
	{ "impress" , "libreoffice --impress"},
	{ "draw" , "libreoffice --draw"},
	{ "base" , "libreoffice --base"},
	}

local designmenu = {
	{ "gschem", "gschem" },
	{ "pcb", "pcb" },
	{ "LTspice", "LTspice" },
	{ "LPCXpresso", "lpcxpresso" },
	{ "Quartus II", "quartus" },
	}

local utilitiesmenu = {
	{ "calc", "gcalctool" }
	}

if host == "frostmere" then
	awesomemenu = awful.util.table.join(awesomemenu, frost_awesome)
	webmenu = awful.util.table.join(frost_web, webmenu)
	mediamenu = awful.util.table.join(mediamenu, frost_media)
	docsmenu = awful.util.table.join(docsmenu, frost_docs)
	graphicsmenu = awful.util.table.join(graphicsmenu, frost_graphics)
elseif host == "silverdrift" then
	awesomemenu = awful.util.table.join(awesomemenu, silver_awesome)
	webmenu = awful.util.table.join(silver_web, webmenu)
	mediamenu = awful.util.table.join(mediamenu, silver_media)
	docsmenu = awful.util.table.join(docsmenu, silver_docs)
	graphicsmenu = awful.util.table.join(graphicsmenu, silver_graphics)
else
	awesomemenu = awful.util.table.join(awesomemenu, ubuntu_awesome)
	adminmenu = awful.util.table.join(adminmenu, ubuntu_admin)
	webmenu = awful.util.table.join(ubuntu_web, webmenu)
	mediamenu = awful.util.table.join(mediamenu, ubuntu_media)
end

local firstitems = {
	{ "awesome", awesomemenu, beautiful.awesome_icon },
	{ "admin", adminmenu },
	{ "web", webmenu },
	{ "im", immenu },
	{ "media", mediamenu },
	{ "docs", docsmenu },
	{ "graphics", graphicsmenu },
	{ "office" , officemenu }
	}
local frost_menu = {
	{ "design", designmenu },
	{ "extra", utilitiesmenu },
	{ "random pic",
		function()
			gears.wallpaper.fit(wallpaperlist[math.random(1, #wallpaperlist)])
		end }
	}
local work_menu = {
	{ "random pic",
		function()
			gears.wallpaper.fit(wallpaperlist[math.random(1, #wallpaperlist)])
		end }
	}
local lastitems = {{ "ranger", terminal .. " -e ranger" }}

if host == "frostmere" or host == "silverdrift" then
	menuitems = awful.util.table.join(firstitems, frost_menu, lastitems)
else
	menuitems = awful.util.table.join(firstitems, work_menu, lastitems)
end

mymainmenu = awful.menu({ items = menuitems })

mylauncher = awful.widget.launcher({ image = beautiful.awesome_icon,
                                     menu = mymainmenu })

