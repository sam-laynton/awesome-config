local awful = require("awful")
local naughty = require("naughty")
-- {{{ Rules
local standardrules = {
    -- All clients will match this rule.
    { rule = { }, properties = { floating = true,
                                 screen = awful.screen.preferred } },
    { rule = { name = "[^%z]" },
      properties = { border_width = beautiful.border_width,
                     border_color = beautiful.border_normal,
                     focus = true,
                     keys = clientkeys,
                     buttons = clientbuttons,
                     size_hints_honor = false,
                     floating = false } },
    { rule = { class = "MPlayer" },
      properties = { floating = true } },
    { rule = { class = "pinentry" },
      properties = { floating = true } },
    { rule = { class = "gimp" },
      properties = { floating = true } },
    { rule = { class = "discord"},
      properties = { tag = tags[2][5] } },
    { rule = { class = "Cisco AnyConnect Secure Mobility Client" },
      properties = { tag = tags[2][4] } },

    { rule = { class = "Microsoft Teams - Preview" },
      properties = { tag = tags[1][5] } },
    { rule = { class = "Microsoft Teams - Preview", name = "Microsoft Teams Notification" },
      properties = { floating = true, focus = false } },

    { rule = { class = "zoom" },
      properties = { tag = tags[1][5] } },

    { rule = { class = "Google-chrome", instance = "teams.microsoft.com" },
      properties = { tag = tags[2][2] } },
    { rule = { class = "Google-chrome", instance = "arm-ce.slack.com" },
      properties = { tag = tags[2][2] } },
    { rule = { class = "Google-chrome", instance = "play.pocketcasts.com__podcasts" },
      properties = { tag = tags[2][3] } },
    { rule = { class = "Google-chrome", instance = "play.google.com__music_listen" },
      properties = { tag = tags[2][3] } },
    { rule = { class = "Google-chrome", instance = "music.youtube.com" },
      properties = { tag = tags[2][3] } },
    -- Set Firefox to always map on tags number 2 of screen 1.
    -- { rule = { class = "Firefox" },
    --   properties = { tag = tags[1][2] } },
}

awful.rules.rules = standardrules

-- }}}


