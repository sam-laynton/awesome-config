local awful = require("awful")
-- Tags definition files

layouts =
{
    awful.layout.suit.floating,
    awful.layout.suit.tile,
    awful.layout.suit.tile.left,
    awful.layout.suit.tile.bottom,
    awful.layout.suit.tile.top,
    awful.layout.suit.fair,
    awful.layout.suit.fair.horizontal,
    awful.layout.suit.spiral,
    awful.layout.suit.spiral.dwindle,
    awful.layout.suit.max,
    awful.layout.suit.max.fullscreen,
    awful.layout.suit.magnifier
}

if screen.count() == 3 then
tags = {
	settings = {
			{ names = { "term", "chrome", "vim", "grep", "term", "ranger", "git", "build", "test" },
				layout = { layouts[2], layouts[2], layouts[2], layouts[2], layouts[2], layouts[2], layouts[2], layouts[2], layouts[2] }},
			{ names = { "windows", "skype", "music", "term", "term", "term", "term", "term", "build" },
				layout = { layouts[2], layouts[2], layouts[2], layouts[2], layouts[2], layouts[2], layouts[2], layouts[2], layouts[2] }},
			{ names = { "windows", "skype", "music", "term", "term", "term", "term", "term", "build" },
				layout = { layouts[2], layouts[2], layouts[2], layouts[2], layouts[2], layouts[2], layouts[2], layouts[2], layouts[2] }},
		   }
       }
elseif screen.count() == 2 then
tags = {
	settings = {
			{ names = { "term", "chrome", "term", "term", "comms", "term", "term", "term", "term" },
				layout = { layouts[2], layouts[2], layouts[2], layouts[2], layouts[2], layouts[2], layouts[2], layouts[2], layouts[2] }},
			{ names = { "email", "slack", "music", "anyconnect", "discord", "term", "term", "term", "term" },
				layout = { layouts[2], layouts[2], layouts[2], layouts[2], layouts[2], layouts[2], layouts[2], layouts[2], layouts[2] }},
		   }
       }
else
tags = {
	settings = {
		{ names = { "term", "chrome", "vim", "files", "music", "term", "term", "term", "other" },
		  layout = { layouts[2], layouts[2], layouts[2], layouts[2], layouts[2], layouts[2], layouts[2], layouts[2], layouts[2] }
		}}}
end

for s = 1, screen.count() do
    -- Each screen has its own tag table.
    tags[s] = awful.tag(tags.settings[s].names, s, tags.settings[s].layout)
end

