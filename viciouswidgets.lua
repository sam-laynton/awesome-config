local vicious = require("vicious")
local widget = require("wibox.widget")
local naughty = require("naughty")
require("colourdef")

-- Seperator
seperator = widget.textbox()
seperator:set_markup(colemph("|"))

seperator_opt = widget.textbox()
if hostname == "frostmere" then
	seperator_opt:set_markup("|")
else
	seperator_opt:set_markup("")
end

-- Helpers to create colour scales
local colour_scale_base = {
-- Low to high
    colbody,
    colvio,
    colblu,
    colcya,
    colgre,
    colyel,
    colora,
    colred,
    colmag
}
local create_colour_scale = function(max_value, alt_scale)
    local scale = {}

    if alt_scale then
        local alt_max = max_value
        for i=#colour_scale_base,2,-1 do
            scale[#scale+1] = { value=alt_max, colour=colour_scale_base[i] }
            alt_max = alt_max / 2
        end
        scale[#scale+1] = { value=0, colour=colour_scale_base[1] }
    else
        for i=#colour_scale_base,1,-1 do
            scale[#scale+1] = { value=(max_value * ((i-1)/#colour_scale_base)), colour=colour_scale_base[i] }
        end
    end
    return scale
end

local units = {["B"] = 1, ["KB"] = 1024, ["MB"] = 1024^2, ["GB"] = 1024^3}
local scale_point = function(name, conversion, use_at)
    return { name=name, conversion=conversion, breakpoint=(conversion*use_at) }
end

local scale_value = function(value, value_scale, value_colour, colour_scale, min_chars_value)
    if not value_colour then
        value_colour = value
    end
    local display_value = " error "
    local display_name = " "
    local display_colour = colcomment

    for i=1,#value_scale do
        if value >= value_scale[i]["breakpoint"] then
            display_value = ("%"..min_chars_value..".1f"):format(value / value_scale[i]["conversion"])
            display_name = value_scale[i]["name"]
            break
        end
    end
    for i=1,#colour_scale do
        if value_colour >= colour_scale[i]["value"] then
            display_colour = colour_scale[i]["colour"]
            break
        end
    end

    return display_colour(display_value.." "..display_name)
end

-- Debug colour helpers
local notify_colour_scale = function(scale)
    local text = ""
    for i=1,#scale do
        text = text..scale[i]["value"]..":"..scale[i]["colour"]("test").."\n"
    end
    naughty.notify({
        text = text,
        font = "Terminus 12",
        timeout = 30
    })
end

colourwidget = widget.textbox()
local colourwidgetmarkup = colbody(" scale:")..colemph(" low ")
for i=1,#colour_scale_base do
    colourwidgetmarkup = colourwidgetmarkup..colour_scale_base[i]("#")
end
colourwidgetmarkup = colourwidgetmarkup..colemph(" high ")
colourwidget:set_markup(colourwidgetmarkup)


local percent_scale = create_colour_scale(100)
-- Cpu widget
cpuwidgettotal = widget.textbox()
vicious.register(cpuwidgettotal, vicious.widgets.cpu,
    function(widget, args)
        for i=1,#percent_scale do
            if args[1] >= percent_scale[i]["value"] then
                highlight = percent_scale[i]["colour"]
                break
            end
        end
        return colbody(" cpu ")..highlight(args[1] .. "% ")
    end, 2)

memwidget_template = function(mem_name, mem_percent_id, mem_value_id)
    -- Value is in MB so modify conversion to account
    local mem_scale = {
        scale_point("GB", units["KB"], 1),
        scale_point("MB", 1, 0)
    }

    return function(widget, args)
        local value = args[mem_value_id]
        local value_colour = args[mem_percent_id]
        if value ~= value then
            return colbody(" no "..mem_name.." ")
        end

        display_value = scale_value(value, mem_scale, value_colour, percent_scale, 3)
        return colbody(" "..mem_name.." ")..display_value.." "
    end
end

-- Ram widget
memwidget = widget.textbox()
vicious.cache(vicious.widgets.mem)
vicious.register(memwidget, vicious.widgets.mem, memwidget_template("mem", 1, 2), 5)

swpwidget = widget.textbox()
vicious.register(swpwidget, vicious.widgets.mem, memwidget_template("swap", 5, 6), 5)


-- Create net widgets
netwidget_template = function(interface, sample_period_sec, average_buffer_size)
    local display_total_scale = {
        scale_point("GB", units["GB"], 1),
        scale_point("MB", units["MB"], 1),
        scale_point("KB", units["KB"], 1),
        scale_point("B", units["B"], 0)
    }
    local display_rate_scale = {
        scale_point("MB/s", units["MB"], 1),
        scale_point("KB/s", units["KB"], 0)
    }
    local rx_rate_colour_scale = create_colour_scale((10 * units["MB"]), true)
    local tx_rate_colour_scale = create_colour_scale((1 * units["MB"]), true)
    local rx_total_colour_scale = create_colour_scale((10 * units["GB"]), true)
    local tx_total_colour_scale = create_colour_scale((1 * units["GB"]), true)

    local last_tx_b = 0
    local last_rx_b = 0

    local prev_tx_bps = {}
    local prev_rx_bps = {}
    local prev_tx_bps_idx = 1
    local prev_rx_bps_idx = 1
    for i=1,average_buffer_size do prev_tx_bps[i] = 0; prev_rx_bps[i] = 0; end

    local get_average = function(array)
        local sum = 0
        for k,v in pairs(array) do
            sum = sum + v
        end
        return sum / #array
    end

    return function(widget, args)
        -- Calculate rate over sample period
        local tx_b = tonumber(args["{"..interface.." tx_b}"])
        local tx_b_diff = tx_b - last_tx_b
        local tx_bps = tx_b_diff / sample_period_sec
        last_tx_b = tx_b

        prev_tx_bps[prev_tx_bps_idx] = tx_bps
        prev_tx_bps_idx = prev_tx_bps_idx + 1; if prev_tx_bps_idx > #prev_tx_bps then prev_tx_bps_idx = 1 end
        local average_tx_bps = get_average(prev_tx_bps)

        local rx_b = tonumber(args["{"..interface.." rx_b}"])
        local rx_b_diff = rx_b - last_rx_b
        local rx_bps = rx_b_diff / sample_period_sec
        last_rx_b = rx_b

        prev_rx_bps[prev_rx_bps_idx] = rx_bps
        prev_rx_bps_idx = prev_rx_bps_idx + 1; if prev_rx_bps_idx > #prev_rx_bps then prev_rx_bps_idx = 1 end
        local average_rx_bps = get_average(prev_rx_bps)


        local display_tx_total = scale_value(tx_b, display_total_scale, nil, tx_total_colour_scale, 3)
        local display_tx_rate = scale_value(average_tx_bps, display_rate_scale, nil, tx_rate_colour_scale, 6)
        local display_rx_total = scale_value(rx_b, display_total_scale, nil, rx_total_colour_scale, 3)
        local display_rx_rate = scale_value(average_rx_bps, display_rate_scale, nil, rx_rate_colour_scale, 6)

        return
            colbody(" tx "..display_tx_total.." at "..display_tx_rate.." ")..
            colemph("|")..
            colbody(" rx "..display_rx_total.." at "..display_rx_rate.." ")
    end
end

netwidget = widget.textbox()
vicious.register(netwidget, vicious.widgets.net, netwidget_template("enp0s31f6", 0.1, 20), 0.1)

-- TODO: Jazz this up, devices in /sys/class/hwmon
cputhermalwidget = widget.textbox()
vicious.register(cputhermalwidget, vicious.widgets.hwmontemp, colbody(" $1 °C "), 5, {"coretemp"})


-- FS widget helpers
local fswidget_line = function(args, mount_point, pretty_name, colour)
    return " " ..
           colbody("/")..
           colcomment(pretty_name.." ")..
           colour(args["{"..mount_point.." used_p}"].."%")..
           colbody(" + ")..
           colour(args["{"..mount_point.." avail_gb}"].." GB ")..
           colbody("free ")
end

local fswidget_template = function(mount_point, pretty_name)
    return function(widget, args)
        if args["{" .. mount_point .. " used_p}"] ~= nil then
            for i=1,#percent_scale do
                if args["{"..mount_point.." used_p}"] >= percent_scale[i]["value"] then
                    return fswidget_line(args, mount_point, pretty_name, percent_scale[i]["colour"])
                end
            end
        else
            return colbody("/")..colcomment(pretty_name.." ")..colbody(" not mounted ")
        end
    end
end

-- Create filesystem widgets
fsrootwidget = widget.textbox()
vicious.cache(vicious.widgets.fs)
vicious.register(fsrootwidget, vicious.widgets.fs, fswidget_template("/", "root"), 621)

fshomewidget = widget.textbox()
vicious.register(fshomewidget, vicious.widgets.fs, fswidget_template("/home", "home"), 621)

fsworkwidget = widget.textbox()
vicious.register(fsworkwidget, vicious.widgets.fs, fswidget_template("/hdd/work", "work"), 621)

fsbminfwidget = widget.textbox()
vicious.register(fsbminfwidget, vicious.widgets.fs, fswidget_template("/nvme/bminf", "bminf"), 621)

fsdockerwidget = widget.textbox()
vicious.register(fsdockerwidget, vicious.widgets.fs, fswidget_template("/docker", "docker"), 621)


-- Create uptime widget
uptimewidget = widget.textbox()
vicious.cache(vicious.widgets.uptime)
vicious.register(uptimewidget, vicious.widgets.uptime, function(widget, args)
    return
        colbody(" up ")..
        colcomment(args[1])..colemph(" days ")..
        colcomment(args[2])..":"..colcomment(("%02d"):format(args[3]))..colemph(" hours ")
end, 2)

local cpu_count = 56
local load_average_scale = create_colour_scale(cpu_count)
local loadaverage_colour = function(args, id, label)
    local value = tonumber(args[id])
    for i=1,#load_average_scale do
        if value >= load_average_scale[i]["value"] then
            return load_average_scale[i]["colour"](" "..args[id].." ")..colemph(label)
        end
    end
end

loadaveragewidget = widget.textbox()
vicious.register(loadaveragewidget, vicious.widgets.uptime,
    function(widget, args)
        return colbody(" load average:")..
            loadaverage_colour(args, 4, "1m")..
            loadaverage_colour(args, 5, "5m")..
            loadaverage_colour(args, 6, "15m")..
            " "
    end, 23)

-- Create timezone widget
local hours_to_sec = function(hours) return hours * 60 * 60 end
norwaytime = widget.textbox()
vicious.register(norwaytime, vicious.widgets.date,
                 colbody(" Norway: ")..
                 colcomment("%R "),
                 2, hours_to_sec(1))

shanghaitime = widget.textbox()
vicious.register(shanghaitime, vicious.widgets.date,
                 colbody(" Shanghai: ")..
                 colcomment("%R "),
                 2, hours_to_sec(7))

