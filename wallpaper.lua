gears = require("gears")
-- {{{ Set wallpaper randomly after random time

-- seed and "pop a few"
math.randomseed( os.time())
for i=1,1000 do tmp=math.random(0,1000) end

x = 0

-- Get the list of files from a directory. Must be all images or folders and non-empty.
function scan_dir(directory)
	local i, filelist, popen = 0, {}, io.popen
	for filename in popen([[find "]] ..directory.. [[" -type f]]):lines() do
	    i = i + 1
	    filelist[i] = filename
	end
	return filelist
end
if screen.count() == 3 then
	wallpaperlist = scan_dir(theme.path .. "wallpaper/triple")
elseif screen.count() == 2 then
	wallpaperlist = scan_dir(theme.path .. "wallpaper/double1200")
else
	wallpaperlist = scan_dir(theme.path .. "wallpaper/single")
end

-- setup the timer
mytimer = timer { timeout = x }
mytimer:connect_signal("timeout", function()

	-- get time of day
	time = os.date('*t')
	-- tell awsetbg to randomly choose a wallpaper from your wallpaper directory
	gears.wallpaper.fit(wallpaperlist[math.random(1, #wallpaperlist)])

	-- stop the time (we don't need multiple instances running at the same time)
	mytimer:stop()

	-- define the interval in which the next wallpaper change should occur in secconds
	x = math.random( 60, 120)

	-- restart the timer
	mytimer.timeout = x
	mytimer:start()
end)

-- initial start when rc.lua is first run
mytimer:start()

-- }}}
