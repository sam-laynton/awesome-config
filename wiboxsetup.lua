local awful = require("awful")
local wibox = require("wibox")
-- {{{ Wibox

-- Load custom and vicious widgets
require("viciouswidgets")
require("customwidgets")

-- Create a textclock widget
mytextclock = awful.widget.textclock()

-- Create a wibox for each screen and add it
mywibox = {}
mypromptbox = {}
mylayoutbox = {}
mytaglist = {}
mytaglist.buttons = awful.util.table.join(
                    awful.button({ }, 1, awful.tag.viewonly),
                    awful.button({ modkey }, 1, awful.client.movetotag),
                    awful.button({ }, 3, awful.tag.viewtoggle),
                    awful.button({ modkey }, 3, awful.client.toggletag),
                    awful.button({ }, 4, function(t) awful.tag.viewnext(awful.tag.getscreen(t)) end),
                    awful.button({ }, 5, function(t) awful.tag.viewprev(awful.tag.getscreen(t)) end)
                    )
mytasklist = {}
mytasklist.buttons = awful.util.table.join(
                     awful.button({ }, 1, function (c)
                                              if c == client.focus then
                                                  c.minimized = true
                                              else
                                                  -- Without this, the following
                                                  -- :isvisible() makes no sense
                                                  c.minimized = false
                                                  if not c:isvisible() then
                                                      awful.tag.viewonly(c:tags()[1])
                                                  end
                                                  -- This will also un-minimize
                                                  -- the client, if needed
                                                  client.focus = c
                                                  c:raise()
                                              end
                                          end),
                     awful.button({ }, 3, function ()
                                              if instance then
                                                  instance:hide()
                                                  instance = nil
                                              else
                                                  instance = awful.menu.clients({ width=250 })
                                              end
                                          end),
                     awful.button({ }, 4, function ()
                                              awful.client.focus.byidx(1)
                                              if client.focus then client.focus:raise() end
                                          end),
                     awful.button({ }, 5, function ()
                                              awful.client.focus.byidx(-1)
                                              if client.focus then client.focus:raise() end
                                          end))

for s = 1, screen.count() do
    -- Create a promptbox for each screen
    mypromptbox[s] = awful.widget.prompt()
    -- Create an imagebox widget which will contains an icon indicating which layout we're using.
    -- We need one layoutbox per screen.
    mylayoutbox[s] = awful.widget.layoutbox(s)
    mylayoutbox[s]:buttons(awful.util.table.join(
                           awful.button({ }, 1, function () awful.layout.inc(layouts, 1) end),
                           awful.button({ }, 3, function () awful.layout.inc(layouts, -1) end),
                           awful.button({ }, 4, function () awful.layout.inc(layouts, 1) end),
                           awful.button({ }, 5, function () awful.layout.inc(layouts, -1) end)))
    -- Create a taglist widget
    mytaglist[s] = awful.widget.taglist(s, awful.widget.taglist.filter.all, mytaglist.buttons)

    -- Create a tasklist widget
    mytasklist[s] = awful.widget.tasklist(s, awful.widget.tasklist.filter.currenttags, mytasklist.buttons)

    -- Create the wibox
    mywibox[s] = awful.wibox({ position = "top", screen = s })

    -- Widgets that are aligned to the left
    local left_layout = wibox.layout.fixed.horizontal()
    left_layout:add(mylauncher)
    left_layout:add(mytaglist[s])
    left_layout:add(mypromptbox[s])

    -- Widgets that are aligned to the right
    local right_layout = wibox.layout.fixed.horizontal()
    if s == 1 then
		right_layout:add(kbdcfg.widget)
		right_layout:add(wibox.widget.systray())
	end
    right_layout:add(mytextclock)
    right_layout:add(mylayoutbox[s])

    -- Now bring it all together (with the tasklist in the middle)
    local layout = wibox.layout.align.horizontal()
    layout:set_left(left_layout)
    layout:set_middle(mytasklist[s])
    layout:set_right(right_layout)

    mywibox[s]:set_widget(layout)
end

bottomwibox = {}

if screen.count() == 2 then
    bottomwibox[1] = awful.wibox({ position = "bottom", screen = 1})
    bottomwibox[2] = awful.wibox({ position = "bottom", screen = 2})
    local left_layout = {}
    local right_layout = {}
    local layout = {}

    for s = 1,2 do
        left_layout[s] = wibox.layout.fixed.horizontal()
        right_layout[s] = wibox.layout.fixed.horizontal()
    end

    left_layout[1]:add(norwaytime)
    left_layout[1]:add(seperator)
    left_layout[1]:add(shanghaitime)
    left_layout[1]:add(seperator)
    left_layout[1]:add(uptimewidget)
    left_layout[1]:add(seperator)
    left_layout[1]:add(cpuwidgettotal)
    left_layout[1]:add(seperator)
    left_layout[1]:add(loadaveragewidget)
    left_layout[1]:add(seperator)
    left_layout[1]:add(cputhermalwidget)

    right_layout[1]:add(colourwidget)
    right_layout[1]:add(seperator)
    right_layout[1]:add(netwidget)

    left_layout[2]:add(fsrootwidget)
    left_layout[2]:add(seperator)
    left_layout[2]:add(fsworkwidget)
    left_layout[2]:add(seperator)
    left_layout[2]:add(fsbminfwidget)
    left_layout[2]:add(seperator)
    left_layout[2]:add(fsdockerwidget)

    right_layout[2]:add(eyewidget)
    right_layout[2]:add(seperator)
    right_layout[2]:add(swpwidget)
    right_layout[2]:add(seperator)
    right_layout[2]:add(memwidget)

    for s = 1,2 do
        layout[s] = wibox.layout.align.horizontal()
        layout[s]:set_left(left_layout[s])
        layout[s]:set_right(right_layout[s])
        bottomwibox[s]:set_widget(layout[s])
    end
else
    bottomwibox[1] = awful.wibox({ position = "bottom", screen = 1})
    local left_layout = wibox.layout.fixed.horizontal()
    local right_layout = wibox.layout.fixed.horizontal()
    local layout = wibox.layout.align.horizontal()

    left_layout:add(uptimewidget)
    left_layout:add(seperator)
    left_layout:add(shanghaitime)
    left_layout:add(seperator)
    left_layout:add(norwaytime)
    left_layout:add(seperator)
    left_layout:add(memwidget)
    left_layout:add(seperator)
    left_layout:add(thermalwidget)
    left_layout:add(seperator)
    left_layout:add(cpuwidgettotal)

    right_layout:add(eyewidget)
    right_layout:add(seperator)
    right_layout:add(netwidget)
    right_layout:add(seperator_opt)

    layout:set_left(left_layout)
    layout:set_right(right_layout)

    bottomwibox[1]:set_widget(layout)
end

-- }}}


